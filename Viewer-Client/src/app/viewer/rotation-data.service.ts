import { Injectable } from '@angular/core';
import { Observable, Observer, Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RotationDataService {
  private subject: Subject<MessageEvent>;

  constructor() { }

  public Connect(url: string): Subject<MessageEvent> {
    if (!this.subject) {
        this.subject = this.createWebSocket(url);
        console.log("Succesfully connected!");
    }
    return this.subject;
  }

  private createWebSocket(url: string) : Subject<MessageEvent> {
    let ws = new WebSocket(url);
    
    let observable = Observable.create((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);
      return ws.close.bind(ws);
    });

    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      }
    };

    return Subject.create(observer, observable);
  }
}
